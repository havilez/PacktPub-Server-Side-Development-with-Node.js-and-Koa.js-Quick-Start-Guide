import {BaseContext} from "koa";
import Router  from 'koa-router';

import { ContactController } from '../controllers/ContactController';


const  contactController = new ContactController();


const router  = new Router();
router
  .get('/', async (ctx:BaseContext) => (ctx.body = 'Welcome to the contacts API!'))
  .get('/contact', contactController.index)
  .post('/contact', contactController.store)
  .get('/contact/:id', contactController.show)
  .put('/contact/:id', contactController.update)
  .delete('/contact/:id', contactController.destroy);

export  {router};
