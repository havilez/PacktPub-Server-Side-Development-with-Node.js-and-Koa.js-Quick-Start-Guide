import Koa from 'koa';
import {Context} from 'koa';

const app = new Koa();


// response

app.use(async ctx => {
  ctx.body = 'Hello World';
});

app.listen(1234,() => {
  console.log('Server is running on port 1234');
})