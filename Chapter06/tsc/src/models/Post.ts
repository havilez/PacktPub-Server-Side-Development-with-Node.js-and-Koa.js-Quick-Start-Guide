import mongoose from 'mongoose';


import { IPost } from '../shared/IPost'

interface IPostModel extends IPost, mongoose.Document { }

const schema = new mongoose.Schema({
 title: {
   type: String,
   required: true
 },
 content: String,
 image: String,
 author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
 },
 createdAt: { type: Date, default: Date.now },
 updatedAt: { type: Date, default: Date.now }
});

// const Post = mongoose.model('Post', schema);
const Post = mongoose.model<IPostModel>("Post", schema);
export { Post }