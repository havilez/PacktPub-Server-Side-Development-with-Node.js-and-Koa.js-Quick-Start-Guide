import { BaseContext } from 'koa';
import { NextFunction } from 'connect';


import {Post} from'../models/Post';

class PostController {
  async index(ctx :BaseContext) {
    const posts = await Post.find()
    .populate('author');
    ctx.state.posts = posts;
    ctx.state.title = 'Home';
    await ctx.render('index');
  }

  async create(ctx :BaseContext) {
    ctx.state.title = 'Create Post';
    await ctx.render('post/create');
  }

  async store(ctx :BaseContext) {
    const { body } = ctx.request;
    const postData = {
      ...body,
      author: ctx.session.user,
      image: 'https://picsum.photos/300/?random'
    };
    const post = await new Post(postData).save();
    ctx.redirect(`/post/${post.id}`);
  }

  async show(ctx :BaseContext) {
    const { id } = ctx.params;
    try {
      const post  = await Post.findById(id).populate('author');
      ctx.state.post = post;
      ctx.state.title = post.title;
    } catch(e) {
      ctx.throw(404, "Post not found");
    }
    await ctx.render('post/show');
  }

  async edit(ctx :BaseContext) {
    const { id } = ctx.params;
    try {
      const post = await Post.findById(id).populate('author');
      ctx.state.post = post;
      ctx.state.title = `Edit Post - ${post.title}`;
    } catch(e) {
      ctx.throw(404, "Post not found");
    }
    await ctx.render('post/edit');
  }

  async update(ctx :BaseContext) {
    const { id } = ctx.params;
    const { body } = ctx.request;
    try {
      const postData = { ...body, createdAt: new Date() }
      const post = await Post.findByIdAndUpdate(id, postData);
      ctx.redirect(`/post/${post.id}`);
    } catch(e) {
      ctx.throw(e);
    }
  }
};

export { PostController }