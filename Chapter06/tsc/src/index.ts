
// Run server for debug in single process without cluster
import {ServerRun} from "./server_run";
 
// start server
const port :number  = process.env.PORT ? parseInt(process.env.PORT) : 3002;

const app = new ServerRun(port);
app.start();