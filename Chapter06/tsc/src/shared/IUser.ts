interface IUser {
    fullName: string;
	email: string;
    password: string;
    createdAt: Date;
    updatedAt: Date;

};

export {IUser};