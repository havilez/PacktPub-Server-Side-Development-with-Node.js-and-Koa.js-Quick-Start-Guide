// ./middleware/router.js
import Router from 'koa-router';

import {authenticated} from './authenticated';
import {guest} from './guest';
import {user} from './user';
import {AuthController} from '../controllers/AuthController';
import {PostController } from '../controllers/PostController';

const  postController = new PostController();
const authController  = new AuthController();

const KoaRouter = new Router();
KoaRouter.use(user());

// base routes.
// authentication not required
KoaRouter
  .get('/', postController.index)
  .get('/post/:id', postController.show);

// auth routes
const auth = new Router()
  .get('/', guest(), authController.index)
  .post('/login', authController.login)
  .post('/register', authController.register)
  .get('/logout', authController.logout);

KoaRouter.use('/auth', auth.routes());

// blog post routes
const posts = new Router();
posts
  .use(authenticated())
  .post('/', postController.store)
  .get('/create', postController.create)
  .put('/:id', postController.update)
  .get('/:id/edit', postController.edit);
KoaRouter.use('/post', posts.routes());

export {KoaRouter}