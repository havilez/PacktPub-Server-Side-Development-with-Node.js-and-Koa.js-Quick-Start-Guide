
// ./middleware/user.ts
import { BaseContext } from 'koa';
import { NextFunction } from 'connect';


function user() {
  return async (ctx :BaseContext, next :NextFunction) => {
    const { user } = ctx.session;
    if (user) ctx.state = { ...ctx.state, user };
    await next();
  };
};

export { user }